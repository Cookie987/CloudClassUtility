<div align="center">

# CloudClassUtility

[![Build status](https://ci.appveyor.com/api/projects/status/5utm8n3uw2ej8nbt?svg=true)](https://ci.appveyor.com/project/Cookie987/cloudclassutility)

[![GitHub release (latest by date)](https://img.shields.io/github/v/release/Cookie987/CloudClassUtility?logo=github)](https://github.com/Cookie987/CloudClassUtility/releases)
![GitHub Release Date](https://img.shields.io/github/release-date/Cookie987/CloudClassUtility)
[![Github stable release downloads](https://img.shields.io/github/downloads/Cookie987/CloudClassUtility/latest/total.svg?label=downloads&logo=github&cacheSeconds=600)](https://github.com/Cookie987/CloudClassUtility/releases/latest)

[![GitHub tag (latest SemVer pre-release)](https://img.shields.io/github/v/tag/Cookie987/CloudClassUtility?color=orange&include_prereleases&label=preview&sort=date&logo=github)](https://github.com/Cookie987/CloudClassUtility/tags)
[![GitHub (Pre-)Release Date](https://img.shields.io/github/release-date-pre/Cookie987/CloudClassUtility?label=preview%20date)](https://github.com/Cookie987/CloudClassUtility/releases)
[![GitHub release (latest by date including pre-releases)](https://img.shields.io/github/downloads-pre/Cookie987/CloudClassUtility/latest/total?label=downloads&logo=github)](https://github.com/Cookie987/CloudClassUtility/releases)

[![License](https://img.shields.io/github/license/Cookie987/CloudClassUtility.svg?label=License&logo=gnu)](https://github.com/Cookie987/CloudClassUtility/blob/main/LICENSE.md)

[![Alt](https://repobeats.axiom.co/api/embed/bd418cb285929486c83bfa7e797eff5ac913da33.svg "Repobeats analytics image")](https://github.com/Cookie987/CloudClassUtility)
</div>

