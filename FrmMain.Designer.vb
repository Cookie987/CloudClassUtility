﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        Me.CrownMenuStrip1 = New ReaLTaiizor.Controls.CrownMenuStrip()
        Me.窗口WToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.主窗口MToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.高级AToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.监视器VToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.设置SToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.日志LToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.查看日志VToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.导出日志ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.清除日志CToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.帮助HToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.检测更新UToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.关于CloudClassUtilityToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.MaterialCard1 = New ReaLTaiizor.Controls.MaterialCard()
        Me.MetroTabControl1 = New ReaLTaiizor.Controls.MetroTabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.BtnChance = New ReaLTaiizor.Controls.MaterialButton()
        Me.BtnNextStep = New ReaLTaiizor.Controls.MaterialButton()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.MaterialLabel1 = New ReaLTaiizor.Controls.MaterialLabel()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.BtnLastStep = New ReaLTaiizor.Controls.MaterialButton()
        Me.MaterialLabel2 = New ReaLTaiizor.Controls.MaterialLabel()
        Me.BtnOK = New ReaLTaiizor.Controls.MaterialButton()
        Me.CrownMenuStrip1.SuspendLayout()
        Me.MaterialCard1.SuspendLayout()
        Me.MetroTabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CrownMenuStrip1
        '
        Me.CrownMenuStrip1.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(63, Byte), Integer), CType(CType(65, Byte), Integer))
        Me.CrownMenuStrip1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(220, Byte), Integer), CType(CType(220, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.CrownMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.窗口WToolStripMenuItem1, Me.日志LToolStripMenuItem, Me.帮助HToolStripMenuItem1})
        Me.CrownMenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.CrownMenuStrip1.MdiWindowListItem = Me.窗口WToolStripMenuItem1
        Me.CrownMenuStrip1.Name = "CrownMenuStrip1"
        Me.CrownMenuStrip1.Padding = New System.Windows.Forms.Padding(3, 2, 0, 2)
        Me.CrownMenuStrip1.Size = New System.Drawing.Size(902, 24)
        Me.CrownMenuStrip1.TabIndex = 3
        Me.CrownMenuStrip1.Text = "CrownMenuStrip1"
        '
        '窗口WToolStripMenuItem1
        '
        Me.窗口WToolStripMenuItem1.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(63, Byte), Integer), CType(CType(65, Byte), Integer))
        Me.窗口WToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.主窗口MToolStripMenuItem1, Me.高级AToolStripMenuItem1, Me.监视器VToolStripMenuItem, Me.设置SToolStripMenuItem1})
        Me.窗口WToolStripMenuItem1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(220, Byte), Integer), CType(CType(220, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.窗口WToolStripMenuItem1.Name = "窗口WToolStripMenuItem1"
        Me.窗口WToolStripMenuItem1.Size = New System.Drawing.Size(62, 20)
        Me.窗口WToolStripMenuItem1.Text = "窗口(&W)"
        '
        '主窗口MToolStripMenuItem1
        '
        Me.主窗口MToolStripMenuItem1.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(63, Byte), Integer), CType(CType(65, Byte), Integer))
        Me.主窗口MToolStripMenuItem1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(220, Byte), Integer), CType(CType(220, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.主窗口MToolStripMenuItem1.Name = "主窗口MToolStripMenuItem1"
        Me.主窗口MToolStripMenuItem1.Size = New System.Drawing.Size(129, 22)
        Me.主窗口MToolStripMenuItem1.Text = "主窗口(&M)"
        '
        '高级AToolStripMenuItem1
        '
        Me.高级AToolStripMenuItem1.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(63, Byte), Integer), CType(CType(65, Byte), Integer))
        Me.高级AToolStripMenuItem1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(220, Byte), Integer), CType(CType(220, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.高级AToolStripMenuItem1.Name = "高级AToolStripMenuItem1"
        Me.高级AToolStripMenuItem1.Size = New System.Drawing.Size(129, 22)
        Me.高级AToolStripMenuItem1.Text = "高级(&A)"
        '
        '监视器VToolStripMenuItem
        '
        Me.监视器VToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(63, Byte), Integer), CType(CType(65, Byte), Integer))
        Me.监视器VToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(CType(CType(220, Byte), Integer), CType(CType(220, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.监视器VToolStripMenuItem.Name = "监视器VToolStripMenuItem"
        Me.监视器VToolStripMenuItem.Size = New System.Drawing.Size(129, 22)
        Me.监视器VToolStripMenuItem.Text = "监视器(&V)"
        '
        '设置SToolStripMenuItem1
        '
        Me.设置SToolStripMenuItem1.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(63, Byte), Integer), CType(CType(65, Byte), Integer))
        Me.设置SToolStripMenuItem1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(220, Byte), Integer), CType(CType(220, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.设置SToolStripMenuItem1.Name = "设置SToolStripMenuItem1"
        Me.设置SToolStripMenuItem1.Size = New System.Drawing.Size(129, 22)
        Me.设置SToolStripMenuItem1.Text = "设置(&S)"
        '
        '日志LToolStripMenuItem
        '
        Me.日志LToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(63, Byte), Integer), CType(CType(65, Byte), Integer))
        Me.日志LToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.查看日志VToolStripMenuItem1, Me.导出日志ToolStripMenuItem, Me.清除日志CToolStripMenuItem1})
        Me.日志LToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(CType(CType(220, Byte), Integer), CType(CType(220, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.日志LToolStripMenuItem.Name = "日志LToolStripMenuItem"
        Me.日志LToolStripMenuItem.Size = New System.Drawing.Size(58, 20)
        Me.日志LToolStripMenuItem.Text = "日志(&L)"
        '
        '查看日志VToolStripMenuItem1
        '
        Me.查看日志VToolStripMenuItem1.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(63, Byte), Integer), CType(CType(65, Byte), Integer))
        Me.查看日志VToolStripMenuItem1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(220, Byte), Integer), CType(CType(220, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.查看日志VToolStripMenuItem1.Name = "查看日志VToolStripMenuItem1"
        Me.查看日志VToolStripMenuItem1.Size = New System.Drawing.Size(139, 22)
        Me.查看日志VToolStripMenuItem1.Text = "查看日志(&V)"
        '
        '导出日志ToolStripMenuItem
        '
        Me.导出日志ToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(63, Byte), Integer), CType(CType(65, Byte), Integer))
        Me.导出日志ToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(CType(CType(220, Byte), Integer), CType(CType(220, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.导出日志ToolStripMenuItem.Name = "导出日志ToolStripMenuItem"
        Me.导出日志ToolStripMenuItem.Size = New System.Drawing.Size(139, 22)
        Me.导出日志ToolStripMenuItem.Text = "导出日志(&E)"
        '
        '清除日志CToolStripMenuItem1
        '
        Me.清除日志CToolStripMenuItem1.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(63, Byte), Integer), CType(CType(65, Byte), Integer))
        Me.清除日志CToolStripMenuItem1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(220, Byte), Integer), CType(CType(220, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.清除日志CToolStripMenuItem1.Name = "清除日志CToolStripMenuItem1"
        Me.清除日志CToolStripMenuItem1.Size = New System.Drawing.Size(139, 22)
        Me.清除日志CToolStripMenuItem1.Text = "清除日志(&C)"
        '
        '帮助HToolStripMenuItem1
        '
        Me.帮助HToolStripMenuItem1.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(63, Byte), Integer), CType(CType(65, Byte), Integer))
        Me.帮助HToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.检测更新UToolStripMenuItem1, Me.关于CloudClassUtilityToolStripMenuItem1})
        Me.帮助HToolStripMenuItem1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(220, Byte), Integer), CType(CType(220, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.帮助HToolStripMenuItem1.Name = "帮助HToolStripMenuItem1"
        Me.帮助HToolStripMenuItem1.Size = New System.Drawing.Size(60, 20)
        Me.帮助HToolStripMenuItem1.Text = "帮助(&H)"
        '
        '检测更新UToolStripMenuItem1
        '
        Me.检测更新UToolStripMenuItem1.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(63, Byte), Integer), CType(CType(65, Byte), Integer))
        Me.检测更新UToolStripMenuItem1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(220, Byte), Integer), CType(CType(220, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.检测更新UToolStripMenuItem1.Name = "检测更新UToolStripMenuItem1"
        Me.检测更新UToolStripMenuItem1.Size = New System.Drawing.Size(198, 22)
        Me.检测更新UToolStripMenuItem1.Text = "检测更新(&U)"
        '
        '关于CloudClassUtilityToolStripMenuItem1
        '
        Me.关于CloudClassUtilityToolStripMenuItem1.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(63, Byte), Integer), CType(CType(65, Byte), Integer))
        Me.关于CloudClassUtilityToolStripMenuItem1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(220, Byte), Integer), CType(CType(220, Byte), Integer), CType(CType(220, Byte), Integer))
        Me.关于CloudClassUtilityToolStripMenuItem1.Name = "关于CloudClassUtilityToolStripMenuItem1"
        Me.关于CloudClassUtilityToolStripMenuItem1.Size = New System.Drawing.Size(198, 22)
        Me.关于CloudClassUtilityToolStripMenuItem1.Text = "关于 CloudClassUtility"
        '
        'MaterialCard1
        '
        Me.MaterialCard1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.MaterialCard1.Controls.Add(Me.MetroTabControl1)
        Me.MaterialCard1.Depth = 0
        Me.MaterialCard1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MaterialCard1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.MaterialCard1.Location = New System.Drawing.Point(0, 24)
        Me.MaterialCard1.Margin = New System.Windows.Forms.Padding(14)
        Me.MaterialCard1.MouseState = ReaLTaiizor.Helper.MaterialDrawHelper.MaterialMouseState.HOVER
        Me.MaterialCard1.Name = "MaterialCard1"
        Me.MaterialCard1.Padding = New System.Windows.Forms.Padding(14)
        Me.MaterialCard1.Size = New System.Drawing.Size(902, 490)
        Me.MaterialCard1.TabIndex = 5
        Me.MaterialCard1.Visible = False
        '
        'MetroTabControl1
        '
        Me.MetroTabControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MetroTabControl1.AnimateEasingType = ReaLTaiizor.[Enum].Metro.EasingType.CubeOut
        Me.MetroTabControl1.AnimateTime = 1000
        Me.MetroTabControl1.BackgroundColor = System.Drawing.Color.White
        Me.MetroTabControl1.Controls.Add(Me.TabPage1)
        Me.MetroTabControl1.Controls.Add(Me.TabPage2)
        Me.MetroTabControl1.ControlsVisible = True
        Me.MetroTabControl1.IsDerivedStyle = True
        Me.MetroTabControl1.ItemSize = New System.Drawing.Size(100, 38)
        Me.MetroTabControl1.Location = New System.Drawing.Point(0, 0)
        Me.MetroTabControl1.MCursor = System.Windows.Forms.Cursors.Hand
        Me.MetroTabControl1.Name = "MetroTabControl1"
        Me.MetroTabControl1.SelectedIndex = 0
        Me.MetroTabControl1.SelectedTextColor = System.Drawing.Color.White
        Me.MetroTabControl1.Size = New System.Drawing.Size(902, 490)
        Me.MetroTabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed
        Me.MetroTabControl1.Speed = 100
        Me.MetroTabControl1.Style = ReaLTaiizor.[Enum].Metro.Style.Light
        Me.MetroTabControl1.StyleManager = Nothing
        Me.MetroTabControl1.TabIndex = 0
        Me.MetroTabControl1.ThemeAuthor = "Taiizor"
        Me.MetroTabControl1.ThemeName = "MetroLight"
        Me.MetroTabControl1.UnselectedTextColor = System.Drawing.Color.Gray
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.BtnChance)
        Me.TabPage1.Controls.Add(Me.BtnNextStep)
        Me.TabPage1.Controls.Add(Me.PictureBox1)
        Me.TabPage1.Controls.Add(Me.MaterialLabel1)
        Me.TabPage1.Location = New System.Drawing.Point(4, 42)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(894, 444)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "步骤1"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'BtnChance
        '
        Me.BtnChance.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.BtnChance.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BtnChance.Depth = 0
        Me.BtnChance.DrawShadows = True
        Me.BtnChance.HighEmphasis = True
        Me.BtnChance.Icon = Nothing
        Me.BtnChance.Location = New System.Drawing.Point(14, 392)
        Me.BtnChance.Margin = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.BtnChance.MouseState = ReaLTaiizor.Helper.MaterialDrawHelper.MaterialMouseState.HOVER
        Me.BtnChance.Name = "BtnChance"
        Me.BtnChance.Size = New System.Drawing.Size(53, 36)
        Me.BtnChance.TabIndex = 11
        Me.BtnChance.Text = "取消"
        Me.BtnChance.TextState = ReaLTaiizor.Controls.MaterialButton.TextStateType.Normal
        Me.BtnChance.Type = ReaLTaiizor.Controls.MaterialButton.MaterialButtonType.Contained
        Me.BtnChance.UseAccentColor = False
        Me.BtnChance.UseVisualStyleBackColor = True
        '
        'BtnNextStep
        '
        Me.BtnNextStep.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtnNextStep.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BtnNextStep.Depth = 0
        Me.BtnNextStep.DrawShadows = True
        Me.BtnNextStep.HighEmphasis = True
        Me.BtnNextStep.Icon = Nothing
        Me.BtnNextStep.Location = New System.Drawing.Point(811, 392)
        Me.BtnNextStep.Margin = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.BtnNextStep.MouseState = ReaLTaiizor.Helper.MaterialDrawHelper.MaterialMouseState.HOVER
        Me.BtnNextStep.Name = "BtnNextStep"
        Me.BtnNextStep.Size = New System.Drawing.Size(69, 36)
        Me.BtnNextStep.TabIndex = 8
        Me.BtnNextStep.Text = "下一步"
        Me.BtnNextStep.TextState = ReaLTaiizor.Controls.MaterialButton.TextStateType.Normal
        Me.BtnNextStep.Type = ReaLTaiizor.Controls.MaterialButton.MaterialButtonType.Contained
        Me.BtnNextStep.UseAccentColor = False
        Me.BtnNextStep.UseVisualStyleBackColor = True
        '
        'PictureBox1
        '
        Me.PictureBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox1.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox1.Image = Global.CloudClassUtility.My.Resources.Resources.warning
        Me.PictureBox1.Location = New System.Drawing.Point(393, 6)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(108, 107)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 7
        Me.PictureBox1.TabStop = False
        '
        'MaterialLabel1
        '
        Me.MaterialLabel1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MaterialLabel1.AutoEllipsis = True
        Me.MaterialLabel1.Depth = 0
        Me.MaterialLabel1.Font = New System.Drawing.Font("Roboto", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.MaterialLabel1.Location = New System.Drawing.Point(13, 3)
        Me.MaterialLabel1.MouseState = ReaLTaiizor.Helper.MaterialDrawHelper.MaterialMouseState.HOVER
        Me.MaterialLabel1.Name = "MaterialLabel1"
        Me.MaterialLabel1.Size = New System.Drawing.Size(868, 383)
        Me.MaterialLabel1.TabIndex = 6
        Me.MaterialLabel1.Text = "..."
        Me.MaterialLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.PictureBox2)
        Me.TabPage2.Controls.Add(Me.BtnLastStep)
        Me.TabPage2.Controls.Add(Me.MaterialLabel2)
        Me.TabPage2.Controls.Add(Me.BtnOK)
        Me.TabPage2.Location = New System.Drawing.Point(4, 42)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(894, 444)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "步骤2"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'PictureBox2
        '
        Me.PictureBox2.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox2.BackColor = System.Drawing.SystemColors.Control
        Me.PictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox2.Image = Global.CloudClassUtility.My.Resources.Resources.warning
        Me.PictureBox2.Location = New System.Drawing.Point(393, 6)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(108, 107)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox2.TabIndex = 15
        Me.PictureBox2.TabStop = False
        '
        'BtnLastStep
        '
        Me.BtnLastStep.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.BtnLastStep.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BtnLastStep.Depth = 0
        Me.BtnLastStep.DrawShadows = True
        Me.BtnLastStep.HighEmphasis = True
        Me.BtnLastStep.Icon = Nothing
        Me.BtnLastStep.Location = New System.Drawing.Point(14, 392)
        Me.BtnLastStep.Margin = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.BtnLastStep.MouseState = ReaLTaiizor.Helper.MaterialDrawHelper.MaterialMouseState.HOVER
        Me.BtnLastStep.Name = "BtnLastStep"
        Me.BtnLastStep.Size = New System.Drawing.Size(69, 36)
        Me.BtnLastStep.TabIndex = 14
        Me.BtnLastStep.Text = "上一步"
        Me.BtnLastStep.TextState = ReaLTaiizor.Controls.MaterialButton.TextStateType.Normal
        Me.BtnLastStep.Type = ReaLTaiizor.Controls.MaterialButton.MaterialButtonType.Contained
        Me.BtnLastStep.UseAccentColor = False
        Me.BtnLastStep.UseVisualStyleBackColor = True
        '
        'MaterialLabel2
        '
        Me.MaterialLabel2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MaterialLabel2.AutoEllipsis = True
        Me.MaterialLabel2.Depth = 0
        Me.MaterialLabel2.Font = New System.Drawing.Font("Roboto", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel)
        Me.MaterialLabel2.Location = New System.Drawing.Point(14, 3)
        Me.MaterialLabel2.MouseState = ReaLTaiizor.Helper.MaterialDrawHelper.MaterialMouseState.HOVER
        Me.MaterialLabel2.Name = "MaterialLabel2"
        Me.MaterialLabel2.Size = New System.Drawing.Size(868, 383)
        Me.MaterialLabel2.TabIndex = 12
        Me.MaterialLabel2.Text = "..."
        Me.MaterialLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'BtnOK
        '
        Me.BtnOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtnOK.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BtnOK.Depth = 0
        Me.BtnOK.DrawShadows = True
        Me.BtnOK.HighEmphasis = True
        Me.BtnOK.Icon = Nothing
        Me.BtnOK.Location = New System.Drawing.Point(827, 392)
        Me.BtnOK.Margin = New System.Windows.Forms.Padding(4, 6, 4, 6)
        Me.BtnOK.MouseState = ReaLTaiizor.Helper.MaterialDrawHelper.MaterialMouseState.HOVER
        Me.BtnOK.Name = "BtnOK"
        Me.BtnOK.Size = New System.Drawing.Size(53, 36)
        Me.BtnOK.TabIndex = 11
        Me.BtnOK.Text = "确认"
        Me.BtnOK.TextState = ReaLTaiizor.Controls.MaterialButton.TextStateType.Normal
        Me.BtnOK.Type = ReaLTaiizor.Controls.MaterialButton.MaterialButtonType.Contained
        Me.BtnOK.UseAccentColor = False
        Me.BtnOK.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(32, Byte), Integer), CType(CType(41, Byte), Integer), CType(CType(50, Byte), Integer))
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(902, 514)
        Me.Controls.Add(Me.MaterialCard1)
        Me.Controls.Add(Me.CrownMenuStrip1)
        Me.DoubleBuffered = True
        Me.IsMdiContainer = True
        Me.MinimumSize = New System.Drawing.Size(126, 50)
        Me.Name = "Form1"
        Me.Text = "CloudClassUtility"
        Me.TransparencyKey = System.Drawing.Color.Fuchsia
        Me.CrownMenuStrip1.ResumeLayout(False)
        Me.CrownMenuStrip1.PerformLayout()
        Me.MaterialCard1.ResumeLayout(False)
        Me.MetroTabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents SaveFileDialog1 As SaveFileDialog
    Friend WithEvents CrownMenuStrip1 As ReaLTaiizor.Controls.CrownMenuStrip
    Friend WithEvents 窗口WToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents 主窗口MToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents 高级AToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents 设置SToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents 日志LToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents 查看日志VToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents 导出日志ToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents 清除日志CToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents 帮助HToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents 检测更新UToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents 关于CloudClassUtilityToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents MaterialCard1 As ReaLTaiizor.Controls.MaterialCard
    Friend WithEvents 监视器VToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents MetroTabControl1 As ReaLTaiizor.Controls.MetroTabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents MaterialLabel1 As ReaLTaiizor.Controls.MaterialLabel
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents BtnNextStep As ReaLTaiizor.Controls.MaterialButton
    Friend WithEvents BtnOK As ReaLTaiizor.Controls.MaterialButton
    Friend WithEvents BtnChance As ReaLTaiizor.Controls.MaterialButton
    Friend WithEvents BtnLastStep As ReaLTaiizor.Controls.MaterialButton
    Friend WithEvents MaterialLabel2 As ReaLTaiizor.Controls.MaterialLabel
    Friend WithEvents PictureBox2 As PictureBox
End Class
